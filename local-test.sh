set -e

cd nrf51-gcc
docker build -t nrf51-gcc:local .
docker run nrf51-gcc:local arm-none-eabi-gcc --version
docker run nrf51-gcc:local sh -c "cd /sdk/examples/peripheral/blinky/pca10028/blank/armgcc && make"
cd ../

cd nrf5-cli
docker build -t nrf5-cli:local .
docker run nrf5-cli:local /cli/nrfjprog/nrfjprog --version
