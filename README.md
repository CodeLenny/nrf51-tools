# NRF51 Development Tools

Containerized tools for developing applications for [Nordic Semiconductor][Nordic]'s nRF51 series - specifically, the [nRF51 Development Kit][nRF51-dk] and [nRF51422][] products.

It's quite possible that other development kits and chips would work with these tools, but other platforms are currently outside the supported scope of this project.

## Images

* **`nrf51-gcc`** contains `gcc`, `make`, [GNU Arm Embedded Toolchain][arm-toolchain], and other tools needed to compile designs for nRF51 chips.

## Other Resources

Most of the platform setup follows [Nordic][]'s [Development with GCC and Eclipse tutorial][nordic-tutorial-7].

[Nordic]: https://www.nordicsemi.com/
[nRF51-dk]: https://www.nordicsemi.com/eng/Products/nRF51-DK
[nRF51422]: https://www.nordicsemi.com/eng/Products/ANT/nRF51422
[nordic-tutorial-7]: https://devzone.nordicsemi.com/tutorials/7/
[arm-toolchain]: https://developer.arm.com/open-source/gnu-toolchain/gnu-rm
